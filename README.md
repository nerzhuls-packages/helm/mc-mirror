# mc-mirror

This charts deploy mc-mirror on kubernetes to mirror 2 buckets once time.

## Warning

Only 2 endpoints can be setup by default
you can add other by modifying configmap.

## Miscellaneous

```text
    ╚⊙ ⊙╝
  ╚═(███)═╝
 ╚═(███)═╝
╚═(███)═╝
╚═(███)═╝
 ╚═(███)═╝
  ╚═(███)═╝
   ╚═(███)═╝
```
