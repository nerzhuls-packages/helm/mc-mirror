package test

import (
	"testing"

	"github.com/gruntwork-io/terratest/modules/helm"
	"github.com/stretchr/testify/assert"
	batchv1 "k8s.io/api/batch/v1"
)

func TestSecret(t *testing.T) {
	helmChartPath := ".."
	deployName := "minio"
	options := &helm.Options{
		SetFiles: map[string]string{"values": "values-test.yaml"},
	}

	output := helm.RenderTemplate(t, options, helmChartPath,
		deployName, []string{"templates/secret.yaml"})

	var testObj batchv1.CronJob
	helm.UnmarshalK8SYaml(t, output, &testObj)

	assert.Equal(t, deployName, testObj.ObjectMeta.Name)
	assert.Contains(t, testObj.ObjectMeta.Labels, "app.kubernetes.io/name")
	assert.Contains(t, testObj.ObjectMeta.Labels, "app.kubernetes.io/instance")
	assert.Contains(t, testObj.ObjectMeta.Labels, "app.kubernetes.io/version")
}
